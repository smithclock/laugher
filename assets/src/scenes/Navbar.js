import React from 'react';
import { Link } from 'react-router-dom';

import logo from './img/logo.png';

export const Navbar = () => {

    return(
        <nav className="navbar">
           <img src={logo} className="logo" />
           <span className="link-menu-style">
               <Link to='/About'>Our team</Link>
           </span>
        </nav>
    )
}