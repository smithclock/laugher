module.exports = {


  friendlyName: 'Multi uploads',


  description: 'action permit to upload multiple image with',


  inputs: {
      
  },


  exits: {
    success : {

      statusCode : 205,
      description : "images succesfully uploads",

    },

    invalid : {
      statusCode : 405,
      description : "images not uploads",
    }
  },


  fn: async function (inputs,exits) {

    // this.req.file('file').upload({
    //   adapter: require('skipper-gridfs'),
    //   uri: 'mongodb://localhost:27017/laugher'
    // }, function (err, filesUploaded) {
    //   if (err) return this.res.serverError(err);
    //   return exits.success({
    //     data : filesUploaded,
    //   })
    // });

    var picturesUploaded;
    // uploads the file 
    this.req.file('file').upload({ 

      // default path where the file will be upload 
      dirname : "F:\\projet\\laugher\\assets\\src\\img\\girls"

    },(err,uploaded) => {

      // when file are not upload  return error
      if(err) return exits.invalid({ message : "images not uplaods"});

      // when file if upload correctly store it to the databases

      let uploadedGenerateName = uploaded[0].fd.split('\\').reverse()[0];

      Picture.create({
          // get the uuid generate name with skipper and store it
          urlPicture :uploadedGenerateName,
          // get the real name of the picture and store it
          pictureName : uploaded[0].filename,
          // initialize the score of the app
          pictureScore : 0

      }).exec((error)=>{
        // if image does not save 
        if (err) return exits.invalid({message : " error database save picture infos"});

        return exits.success({ message : "image successfuly save"});

      });

    })

  }


};
