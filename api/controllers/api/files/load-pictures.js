module.exports = {


  friendlyName: 'Load pictures',


  description: '',


  inputs: {

  },


  exits: {
      invalid : {
        statusCode : 403,
        description : "no pictures loaded"
      },

      success : {
        statusCode : 200,
        description : "all pictures is loaded",
      }
  },


  fn: async function (inputs,exits) {

    const ALLPictures = await Picture.find({});
    return exits.success({
      message : "pictures full loaded",
      data : ALLPictures
    });

  }


};
