module.exports = {


  friendlyName: 'Update',


  description: 'Update notes.',


  inputs: {
    label: {
      type : 'string',
      required : false,
    },

    content : {
      type : 'string',
      required : false,
    },

    noteId : {
      type : 'string',
      required : true,
    }
  },


  exits: {

    invalid : {
      statusCode : 409,
      description : "notes failed update"
    },

    success : {
      statusCode : 201,
      description : "note successfully update"
    }



  },


  fn: async function (inputs, exits) {

    let notesUpdated = await Notes.update({
        id : inputs.noteId
    }).set({
      label : inputs.label,
      content : inputs.content,
    }).fetch();


    if(!notesUpdated){
      return exits.invalid({
        message : "note error update",
      });
    }

    return exits.success({
      message : "notes successfully update",
      data : notesUpdated
    });

  }


};
