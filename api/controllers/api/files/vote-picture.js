module.exports = {


  friendlyName: 'Vote picture',


  description: 'permit a visitors to vote',


  inputs: {
    id : {
      type : 'string',
      required : true,
    },

    note : {
      type : 'number',
      required : true,
    },

  },


  exits: {
    invalid : {
      statusCode : 409,
      description : "vote not accepted"
    },

    success : {
      statusCode : 200,
      description : "vote accepted",
    }
  },


  fn: async function (inputs,exits) {

    var pic = await Picture.update({
       _id : inputs.id
    }).set({
      pictureScore : inputs.note
    })
    .exec((error)=>{
      if(error) return exits.invalid({message : "error databases for add votes"});

      return exits.success({ message : "vote completed"});

    });

  }

};
