import { combineReducers, createStore } from 'redux';
import thunk from "redux-thunk";
import { applyMiddleware } from "redux";

import pictures from './Pictures/PictureReducer';

const laugherApp = combineReducers({
   pictures,
  });

  
export default createStore(laugherApp , applyMiddleware(thunk));