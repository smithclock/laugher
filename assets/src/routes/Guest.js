import React from 'react';
import ReactDOM from 'react-dom';
import  { BrowserRouter, Route , Switch } from 'react-router-dom';

import {WholeLandingPage} from '../scenes/WholeLandingPage/components/LandingPage';
import {About} from '../scenes/About/components/About';


export class Guest extends React.Component {

    render(){
        return(
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={WholeLandingPage} />
                    <Route exact path={"/About"} component={About} />
                </Switch>
            </BrowserRouter>
        )
    }
}