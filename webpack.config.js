const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    entry: ['@babel/polyfill','./assets/src/index.js']
  },
  output: {
    path: __dirname + '/.tmp/public',
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/
      },
      {
        use: ['style-loader', 'css-loader'],
        test: /\.css$/i,

      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use : [
          {
            loader : 'file-loader',
          },
        ],
      }
    ]
  },

  devServer: {
    historyApiFallback: true,
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'assets/src/index.html'
    })
  ]
};