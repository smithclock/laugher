module.exports = {


  friendlyName: 'Index',


  description: 'Index notes.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    let notes = await Notes.find({});
    return notes;

  }


};
