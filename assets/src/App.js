import React from 'react';

import {Provider, connect } from 'react-redux';
import Store from "./reducer/index";
import { Guest } from './routes/Guest';
import '../styles/app.css';

console.log('hey there');

class Main extends React.Component {

    constructor(props)
    {
        super(props)

        this.state = {

            pictures : this.props.pictures,
        }
    }

    render() {
        return(
                <Guest/>
        )
        
    }
}

const mapStateToProps = state => {
    return {
      pictures : state.pictures,
    }
  }

const mapDispatchToProps = dispatch => {
    return {
     
    }
  }

let MainContainer = connect(mapStateToProps,mapDispatchToProps)(Main);

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name : "eric domngang"
        }
    }

    render() {
        return (
            <Provider store={Store}>
                <MainContainer />
            </Provider>
        )
    }
}

export default (App);