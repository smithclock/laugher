import React from 'react';
import { Navbar } from '../../Navbar';
import { PictureLayout} from '../../Pictures/components/PicturesLayout';

export class WholeLandingPage extends React.Component {

    render(){
        return(
            <div className="container-Landing-page">
                <Navbar/>
                <h1>Let know who is the ugly person of 2019</h1>
                <h2>Choose the best picture</h2>
                <PictureLayout/>
            </div>
        )
    }
}