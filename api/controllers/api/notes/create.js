module.exports = {


  friendlyName: 'Create',


  description: 'Create notes.',


  inputs: {

    label : {
      type : 'string',
      required : true
    },

    content : {
      type : 'string',
      required : true
    },

  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'notes create error' // this will not go in response
    },

    success : {
      statusCode : 200,
      description : 'notes create succesfully'
    }

  },


  fn: async function (inputs,exits) {

    // All done

    var notesRecord = await Notes.create({
      label : inputs.label,
      content : inputs.content,
    }).fetch();

    if(!notesRecord)
    {
      return exits.invalid({
        message : 'invalid , problem creating notes',
      });

    }

    return exits.success({
      message : 'notes has been created',
      data : notesRecord,
    });
    
  }


};
